import pandas as pd
import matplotlib.pyplot as plt
from sklearn import model_selection, linear_model, metrics
from sklearn import svm
from sklearn import neighbors
from sklearn import tree

import time
import numpy as np

# Task 1
def pre_processing_and_visualisation():

    # Loading the dataset and separating labels from the feature vectors
    product_images = pd.read_csv('product_images.csv')

    images_data = product_images.drop(['label'], axis=1)
    images_labels = product_images['label']

    # No. of "sneakers" images in samples, label of 0 in csv denotes "sneaker"
    no_of_sneakers = len(product_images[images_labels == 0])
    print(no_of_sneakers, " of the samples are images of sneakers.\n")

    # No. of "ankle boots" images in samples, label of 1 in csv denotes "ankle boot"
    no_of_ankle_boots = len(product_images[images_labels == 1])
    print(no_of_ankle_boots, " of the samples are images of ankle boots.\n")

    # One image for each class
    plt.figure()

    # Image of a sneaker (index 30 being the single, selected image) 
    plt.imshow(images_data.iloc[[images_labels[images_labels==0].index[30]]].values.reshape(28,28))
    plt.show()

    # Image of an ankle boot
    plt.imshow(images_data.iloc[[images_labels[images_labels==1].index[30]]].values.reshape(28,28))
    plt.show()

    return images_data, images_labels

# Task 2
def evaluation_procedure(images_data, images_labels, number_of_samples, classifier):

    # Parameterize the datasets based on "number_of_samples" passed in as argument
    data_samples = images_data.sample(n = number_of_samples)
    labels_samples = images_labels[data_samples.index]

    # The classifier passed into the function will be used
    clf = classifier
    classifier_name = clf.__class__.__name__

    # For readability
    print("=" * 60)
    print("The " + classifier_name + " classifier will be used for this evaluation, using ", number_of_samples, "samples")
    print("=" * 60 + "\n")

    # Array to store training times per training sample
    training_times = []
    # Array to store prediction times per evaluation sample
    prediction_times = []

    # Arrays to store confusion matrix
    true_positives = []
    false_positives = []
    false_negatives = []
    true_negatives = []

    # Array to store the accuracies scores of the classification
    accuracies = []

    # K-Fold cross-validation to split data
    kf = model_selection.KFold(n_splits=5, shuffle=True)
    for train_index, test_index in kf.split(data_samples, labels_samples):

        # Training subset
        training_data = data_samples.iloc[train_index]
        training_labels = labels_samples.iloc[train_index]

        # Evaluation subset
        test_data = data_samples.iloc[test_index]
        test_labels = labels_samples.iloc[test_index]

        # Processing time required for training
        train_timer_start = time.time()
        clf.fit(training_data, training_labels)
        train_timer_end = time.time()

        # Add the difference in times (the elapsed time) to the list of training times
        training_times.append(train_timer_end - train_timer_start)
        print(f'Processing time for training the {classifier_name} classifier this fold is: {(train_timer_end - train_timer_start):.5f}')

        # Processing time required for prediction
        prediction_timer_start = time.time()
        predictions = clf.predict(test_data)
        prediction_timer_end = time.time()

        # Add the difference in times (the elapsed time) to the list of prediction times
        prediction_times.append(prediction_timer_end - prediction_timer_start)

        print(f'Processing time for prediction with the {classifier_name} classifier this fold is: {(prediction_timer_end - prediction_timer_start):.5f}')

        # Confusion matrix, positions correlate to their "index", rather than the true prediction table of (1,1) being true positive for example 
        confusion = metrics.confusion_matrix(test_labels, predictions)
        print("\nConfusion matrix for current fold is: \n", confusion, "\n")
        print(f"True Positives this fold: {confusion[0,0]} ({(confusion[0,0] / len(test_labels) * 100):.2f}%) (Correctly predicted as Sneakers)")
        print(f"False Negatives this fold: {confusion[0,1]} ({(confusion[0,1] / len(test_labels) * 100):.2f}%)")
        print(f"False Positives this fold: {confusion[1,0]} ({(confusion[1,0] / len(test_labels) * 100):.2f}%)")
        print(f"True Negatives this fold: {confusion[1,1]} ({(confusion[1,1] / len(test_labels) * 100):.2f}%) (Correctly predicted as Ankle Boots)")

        # Append our confusion matrix to our list of results
        true_positives.append(confusion[0,0])
        false_negatives.append(confusion[0,1])
        false_positives.append(confusion[1,0])
        true_negatives.append(confusion[1,1])


        # Accuracy score, calculates the accuracy by summing the true positives and negatives, and dividing them by the total number of evaluation labels (for given fold) 
        accuracy = metrics.accuracy_score(test_labels, predictions)
        print(f'\nAccuracy of the {classifier_name} for this fold is: {accuracy}, or {(accuracy*100):.2f}%\n')
        print("-"*60)

        # As with the timings, add the accuracy to the list of accuracies
        accuracies.append(accuracy)

    print("RESULTS")
    print("-"*60)

    # Minimum, maximum and average training times 
    print(f'The minimum training time for the {classifier_name} classifier using {number_of_samples} samples is: {min(training_times):.6f}')
    print(f'The maximum training time for the {classifier_name} classifier using {number_of_samples} samples is: {max(training_times):.6f}')
    print(f'The average training time for the {classifier_name} classifier using {number_of_samples} samples is: {np.average(training_times):.6f}\n')

    # Minimum, maximum and average prediction times
    print(f'The minimum prediction time for the {classifier_name} classifier using {number_of_samples} samples is: {min(prediction_times):.6f}')
    print(f'The maximum prediction time for the {classifier_name} classifier using {number_of_samples} samples is: {max(prediction_times):.6f}')
    print(f'The average prediction time for the {classifier_name} classifier using {number_of_samples} samples is: {np.average(prediction_times):.6f}\n')

    # Minimum, maximum and average prediction accuracies
    print(f'The minimum prediction accuracy for the {classifier_name} classifier using {number_of_samples} samples is: {min(accuracies):.6f} ({min(accuracies) * 100:.2f}%)')
    print(f'The maximum prediction accuracy for the {classifier_name} classifier using {number_of_samples} samples is: {max(accuracies):.6f} ({max(accuracies) * 100:.2f}%)')
    print(f'The average prediction accuracy for the {classifier_name} classifier using {number_of_samples} samples is: {np.average(accuracies):.6f} ({np.average(accuracies) * 100:.2f}%)\n')

    # Final confusion matrix for the classifier for the given sample
    print(f'True Positives for this sample size: {np.sum(true_positives)} out of {number_of_samples} ({((np.sum(true_positives)/number_of_samples)*100):.2f}%)')
    print(f'False Negatives for this sample size: {np.sum(false_negatives)} out of {number_of_samples} ({((np.sum(false_negatives)/number_of_samples)*100):.2f}%)')
    print(f'False Positives for this sample size: {np.sum(false_positives)} out of {number_of_samples} ({((np.sum(false_positives)/number_of_samples)*100):.2f}%)')
    print(f'True Negatives for this sample size: {np.sum(true_negatives)} out of {number_of_samples} ({((np.sum(true_negatives)/number_of_samples)*100):.2f}%)\n\n\n')

    return np.average(training_times), np.average(prediction_times), np.average(accuracies);

# Task 3
def perceptron(images_data, images_labels, sample_size):

    training_runtimes = []
    prediction_runtimes = []
    average_accuracies_per_sample_size = []

    for sample in sample_size:
        training_run, prediction_run, accuracy = evaluation_procedure(images_data, images_labels, sample, linear_model.Perceptron())

        # Add the average times per sample to a list to plot them on a graph
        training_runtimes.append(training_run)
        prediction_runtimes.append(prediction_run)
        average_accuracies_per_sample_size.append(accuracy)

    index_of_sample_with_highest_accuracy = np.argmax(average_accuracies_per_sample_size)
    print(f"The highest_accuracy achieved was {max(average_accuracies_per_sample_size)}% with a sample size of {sample_size[index_of_sample_with_highest_accuracy]}")
    print(f"The mean prediction accuracy for this classifier is {np.average(average_accuracies_per_sample_size)}")
    plt.title("Perceptron Classifier")
    plt.xlabel("No. of Samples")
    plt.ylabel("Runtimes in Seconds")

    plt.plot(sample_size, training_runtimes)
    plt.plot(sample_size, prediction_runtimes)

    plt.legend(["Training time", "Prediction time"], loc = "upper left")

    plt.show()
    return training_runtimes, prediction_runtimes, average_accuracies_per_sample_size, index_of_sample_with_highest_accuracy;

# Task 4
def support_vector_machine(images_data, images_labels, sample_size):

    training_runtimes = []
    prediction_runtimes = []
    average_accuracies_per_sample_size = []

    # Gamma values
    gammas = [1e-1, 1e-2, 1e-3, 1e-4, 1e-5, 1e-6, 1e-7, 1e-8]

    # Evaluate the SVM classifier using a radial basis function kernel, and different values for 𝛾 (gamma)
    varying_gamma_accuracies = []

    for gamma in gammas:
        print("=" * 60)
        print("The current gamma (𝛾) value to be evaluated is ", gamma)
        print("=" * 60 + "\n")
        # Change sample size for gamma len etc.
        training_run, prediction_run, accuracy = evaluation_procedure(images_data, images_labels, 2000, svm.SVC(kernel="rbf", gamma=gamma))
        varying_gamma_accuracies.append(accuracy)

    # Selecting the gamma which provided the highest accuracy score
    max_gamma = max(varying_gamma_accuracies)
    index_of_best_gamma = varying_gamma_accuracies.index(max_gamma)
    print("\nA good value for gamma would be: ", gammas[index_of_best_gamma])
    
    # As we evaluate each sample, we use the "best" value for gamma
    for sample in sample_size:
        training_run, prediction_run, accuracy = evaluation_procedure(images_data, images_labels, sample, svm.SVC(kernel="rbf", gamma=gammas[index_of_best_gamma]))

        # Add the average times per sample to a list to plot them on a graph
        training_runtimes.append(training_run)
        prediction_runtimes.append(prediction_run)
        average_accuracies_per_sample_size.append(accuracy)

    index_of_sample_with_highest_accuracy = np.argmax(average_accuracies_per_sample_size)
    print(f"The highest_accuracy achieved was {max(average_accuracies_per_sample_size)}% with a sample size of {sample_size[index_of_sample_with_highest_accuracy]}")
    print(f"The mean prediction accuracy for this classifier is {np.average(average_accuracies_per_sample_size)}")
    plt.title("SVM Classifier")
    plt.xlabel("No. of Samples")
    plt.ylabel("Runtimes in Seconds")

    plt.plot(sample_size, training_runtimes)
    plt.plot(sample_size, prediction_runtimes)

    plt.legend(["Training time", "Prediction time"], loc = "upper left")

    plt.show()

    return training_runtimes, prediction_runtimes, average_accuracies_per_sample_size, index_of_sample_with_highest_accuracy;

# Task 5
def k_nearest_neighbours(images_data, images_labels, sample_size):

    training_runtimes = []
    prediction_runtimes = []
    average_accuracies_per_sample_size = []

    # Evaluate the kNN classifier using 1000 samples, and the save each mean average accuracy from the k-folds to this array
    k_value_accuracies = []

    for k in range(1,51):
        print("=" * 60)
        print("The current k value to be evaluated is ", k)
        print("=" * 60 + "\n")
        training_run, prediction_run, accuracy = evaluation_procedure(images_data, images_labels, len(images_data), neighbors.KNeighborsClassifier(n_neighbors = k))

        # Every k value run gives back the mean accuracy of the folds
        k_value_accuracies.append(accuracy)

    # Selecting the k-value which provided the highest mean accuracy score
    best_k = np.argmax(k_value_accuracies)+1
    print("\nA good value for k would be: ", best_k)

    # As we evaluate each sample, we use the "best" value for k
    for sample in sample_size:
        training_run, prediction_run, accuracy = evaluation_procedure(images_data, images_labels, sample, neighbors.KNeighborsClassifier(n_neighbors = best_k))

        # Add the average times per sample to a list to plot them on a graph
        training_runtimes.append(training_run)
        prediction_runtimes.append(prediction_run)
        average_accuracies_per_sample_size.append(accuracy)

    index_of_sample_with_highest_accuracy = np.argmax(average_accuracies_per_sample_size)
    print(f"The highest_accuracy achieved was {max(average_accuracies_per_sample_size)}% with a sample size of {sample_size[index_of_sample_with_highest_accuracy]}")
    print(f"The mean prediction accuracy for this classifier is {np.average(average_accuracies_per_sample_size)}")
    plt.title(f"kNN Classifier, k-value = {best_k}")
    plt.xlabel("No. of Samples")
    plt.ylabel("Runtimes in Seconds")

    plt.plot(sample_size, training_runtimes)
    plt.plot(sample_size, prediction_runtimes)

    plt.legend(["Training time", "Prediction time"], loc = "upper left")

    plt.show()

    return training_runtimes, prediction_runtimes, average_accuracies_per_sample_size, index_of_sample_with_highest_accuracy;

# Task 6
def decision_trees(images_data, images_labels, sample_size):

    training_runtimes = []
    prediction_runtimes = []
    average_accuracies_per_sample_size = []

    training_run, prediction_run, accuracy = evaluation_procedure(images_data, images_labels, len(images_data), tree.DecisionTreeClassifier())

    for sample in sample_size:
        training_run, prediction_run, accuracy = evaluation_procedure(images_data, images_labels, sample, tree.DecisionTreeClassifier())

        # Add the average times per sample to a list to plot them on a graph
        training_runtimes.append(training_run)
        prediction_runtimes.append(prediction_run)
        average_accuracies_per_sample_size.append(accuracy)

    index_of_sample_with_highest_accuracy = np.argmax(average_accuracies_per_sample_size)
    print(f"The highest_accuracy achieved was {max(average_accuracies_per_sample_size)}% with a sample size of {sample_size[index_of_sample_with_highest_accuracy]}")
    print(f"The mean prediction accuracy for this classifier is {np.average(average_accuracies_per_sample_size)}")
    plt.title("Decision Tree Classifier")
    plt.xlabel("No. of Samples")
    plt.ylabel("Runtimes in Seconds")

    plt.plot(sample_size, training_runtimes)
    plt.plot(sample_size, prediction_runtimes)

    plt.legend(["Training time", "Prediction time"], loc = "upper left")

    plt.show()

    return training_runtimes, prediction_runtimes, average_accuracies_per_sample_size, index_of_sample_with_highest_accuracy;


# Task 7
def comparisons(sample_size, perceptron_training_times, perceptron_prediction_times, perceptron_accuracies, perceptron_best_sample_index, svm_training_times, svm_prediction_times, svm_accuracies, svm_best_sample_index, knn_training_times, knn_prediction_times, knn_accuracies, knn_best_sample_index, decision_tree_training_times, decision_tree_prediction_times, decision_tree_accuracies, decision_tree_best_sample_index):

    # Each classifier's training and prediction times, for each given sample, on a single graph for comparison
    plt.title("Training and Prediction Time Comparison")
    plt.xlabel("No.of Samples")
    plt.ylabel("Runtimes in Seconds")
    
    plt.plot(sample_size, perceptron_training_times)
    plt.plot(sample_size, perceptron_prediction_times)
    
    plt.plot(sample_size, svm_training_times)
    plt.plot(sample_size, svm_prediction_times)
    
    plt.plot(sample_size, knn_training_times)
    plt.plot(sample_size, knn_prediction_times)
    
    plt.plot(sample_size, decision_tree_training_times)
    plt.plot(sample_size, decision_tree_prediction_times)
    
    plt.legend(["Perceptron Train Time", "Perceptron Prediction Time", "SVM Train Time", "SVM Prediction Time","kNN Train Time", "kNN Prediction Time", "Decision Tree Train Time", "Decision Tree Prediction Time"], loc = "upper left", fontsize=8)
    
    plt.show()
    
    # Each classifier's accuracy score and prediction times, for each given sample, on a single graph for comparison
    
    plt.title("Accuracy Score Comparison")
    plt.xlabel("No.of Samples")
    plt.ylabel("Accuracy Score")
    
    plt.plot(sample_size, perceptron_accuracies)
    plt.plot(sample_size, svm_accuracies)
    plt.plot(sample_size, knn_accuracies)
    plt.plot(sample_size, decision_tree_accuracies)
    
    plt.legend(["Perceptron Accuracy", "SVM Accuracy", "kNN Accuracy", "Decision Tree Accuracy"], loc = "upper left", fontsize=6)
    
    # Show the values of each accuracy on the graph, rounded to 3 decimal places for readability
    for index in range(len(sample_size)):    
        plt.text((sample_size[index]), round(perceptron_accuracies[index], 4), round(perceptron_accuracies[index], 4), size=8)
        
    for index in range(len(sample_size)):    
        plt.text((sample_size[index]), round(svm_accuracies[index], 4), round(svm_accuracies[index], 4), size=8)
        
    for index in range(len(sample_size)):    
        plt.text((sample_size[index]), round(knn_accuracies[index], 4), round(knn_accuracies[index], 4), size=8)
        
    for index in range(len(sample_size)):    
        plt.text((sample_size[index]), round(decision_tree_accuracies[index], 4), round(decision_tree_accuracies[index], 4), size=8)
    
    plt.show()

    return 0;

# Main Function
def main():

    # For the purposes of varying sample sizes for each classifier, can adjust as required
    sample_size = [2000,4000,6000,8000,10000,12000,14000]

    print()

    # Task 1 (pre-processing and visualisation, required for other tasks, can comment out other tasks as needed)
    images_data, images_labels = pre_processing_and_visualisation()

    # Task 2 and Task 3 (evaluation procedure, test of function using perceptron with 1000 samples)
    evaluation_procedure(images_data, images_labels, 1000, linear_model.Perceptron())

    # Task 3 (Perceptron, with varying samples)
    perceptron_training_times, perceptron_prediction_times, perceptron_accuracies, perceptron_best_sample_index = perceptron(images_data, images_labels, sample_size)

    # Task 4 (Support Vector Machine)
    svm_training_times, svm_prediction_times, svm_accuracies, svm_best_sample_index = support_vector_machine(images_data, images_labels, sample_size)

    # Task 5 (k-nearest Neighbours)
    knn_training_times, knn_prediction_times, knn_accuracies, knn_best_sample_index = k_nearest_neighbours(images_data, images_labels, sample_size)

    # Task 6 (Decision trees)
    decision_tree_training_times, decision_tree_prediction_times, decision_tree_accuracies, decision_tree_best_sample_index = decision_trees(images_data, images_labels, sample_size)

    # Task 7 (comparison)
    comparisons(sample_size, perceptron_training_times, perceptron_prediction_times, perceptron_accuracies, perceptron_best_sample_index, svm_training_times, svm_prediction_times, svm_accuracies, svm_best_sample_index, knn_training_times, knn_prediction_times, knn_accuracies, knn_best_sample_index, decision_tree_training_times, decision_tree_prediction_times, decision_tree_accuracies, decision_tree_best_sample_index)


main()


